﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunController : MonoBehaviour
{
    private GameObject[] guns;
    private int index = 0;
    private bool clicked = false;
    //public GameObject[] bullet;
    //private GameObject[] shotPoint;
    //private float timeBtwShots;
    //public float startTimeBtwShots;

    // Start is called before the first frame update
    void Start()
    {
        guns = GameObject.FindGameObjectsWithTag("Gun");
        //shotPoint = GameObject.FindGameObjectsWithTag("ShotPoint");
        foreach (GameObject g in guns)
        {
            /*if (g.activeSelf == true)
            {
                g.SetActive(false);
            }*/
            if (g.GetComponent<SpriteRenderer>().enabled)
            {
                g.GetComponent<SpriteRenderer>().enabled = false;
            }
        }
        //guns[index].SetActive(true);
        guns[index].GetComponent<SpriteRenderer>().enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float rot = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot);
        SwitchGun();
        FlipGun();

        /*if (timeBtwShots <= 0)
        {
            //ShootGun();
            timeBtwShots = startTimeBtwShots;
        }
        else
        {
            timeBtwShots -= Time.deltaTime;
        }*/
    }

   /* void ShootGun()
    {
        Quaternion rotate = Quaternion.Euler(0f, 0f, 90f);
        if (Input.GetMouseButtonDown(0))
        {
            Instantiate(bullet[index], shotPoint[index].transform.position, shotPoint[index].transform.rotation);
        }
    }*/
    void FlipGun()
    {
        if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x < transform.position.x)
        {
            guns[index].GetComponent<SpriteRenderer>().flipY = true;
        }
        if (Camera.main.ScreenToWorldPoint(Input.mousePosition).x > transform.position.x)
        {
            guns[index].GetComponent<SpriteRenderer>().flipY = false;
        }
    }

    void SwitchGun()
    {
        if (index < 2)
        {
            if (Input.GetMouseButtonDown(1))
            {
                index += 1;
                clicked = true;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(1))
            {
                index = 0;
                clicked = true;
            }
        }

        if (clicked == true)
        {
            foreach (GameObject g in guns)
            {
                if (g.GetComponent<SpriteRenderer>().enabled)// g.activeSelf == true)
                {
                    //g.SetActive(false);
                    g.GetComponent<SpriteRenderer>().enabled = false;
                }
            }

            //guns[index].SetActive(true);
            guns[index].GetComponent<SpriteRenderer>().enabled = true;
        }
    }
}
