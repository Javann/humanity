﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float speed = 5f;

    private Rigidbody2D rb;
    public float dashSpeed;
    private float dashTime;
    public float startDashTime;
    private List<string> direction = new List<string>();

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        dashTime = startDashTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("w"))
        {
            transform.Translate(Vector2.up * Time.deltaTime * speed);
        }
        if (Input.GetKey("a"))
        {
            transform.Translate(Vector2.left * Time.deltaTime * speed);
        }
        if (Input.GetKey("s"))
        {
            transform.Translate(Vector2.down * Time.deltaTime * speed);
        }
        if (Input.GetKey("d"))
        {
            transform.Translate(Vector2.right * Time.deltaTime * speed);
        }

        if (direction.Count == 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (Input.GetKey("w"))
                {
                    direction.Add("up");
                }
                if (Input.GetKey("a"))
                {
                    direction.Add("left");
                }
                if (Input.GetKey("s"))
                {
                    direction.Add("down");
                }
                if (Input.GetKey("d"))
                {
                    direction.Add("right");
                }
                foreach (string i in direction)
                {
                    Debug.Log(i);
                }
            }
        }
        else
        {
            if (dashTime <= 0)
            {
                direction.Clear();
                Debug.Log(direction.Count);
                dashTime = startDashTime;
                rb.velocity = Vector2.zero;
            }
            else
            {
                dashTime -= Time.deltaTime;

                if (direction.Contains("up"))
                {
                    rb.velocity = new Vector2(rb.velocity.x, dashSpeed);
                }
                else if (direction.Contains("down"))
                {
                    rb.velocity = new Vector2(rb.velocity.x, -dashSpeed);
                }
                if (direction.Contains("left"))
                {
                    rb.velocity = new Vector2(-dashSpeed, rb.velocity.y);
                }
                else if (direction.Contains("right"))
                {
                    rb.velocity = new Vector2(dashSpeed, rb.velocity.y);
                }
            }
        }
    }
}