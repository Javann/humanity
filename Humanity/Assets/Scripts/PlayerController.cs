﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    private Rigidbody2D rb;
    //private float dashSpeed;
    //private float dashTime;
    public float startDashTime;
    private float directionX;
    private float directionY;
    //private bool dashing = false;
    private bool facingRight = true;

    private Vector2 moveInput;

    private Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        /*dashSpeed = moveSpeed * 6;
        dashTime = startDashTime;*/

        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Flip(rb.velocity.x);

        if (Input.GetKey("w") || Input.GetKey("a") || Input.GetKey("s") || Input.GetKey("d"))
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }
    }

    void FixedUpdate()
    {
        //if (!dashing)
        //{
        rb.velocity = moveInput.normalized * moveSpeed;
        //}
    }

    /*void Dash()
    {
        if (directionX == 0 && directionY == 0 && Input.GetKeyDown(KeyCode.Space))
        {
            if (Input.GetKey("w"))
            {
                directionY = 1;
            }
            else if (Input.GetKey("s"))
            {
                directionY = 2;
            }

            if (Input.GetKey("a"))
            {
                directionX = 1;
            }
            else if (Input.GetKey("d"))
            {
                directionX = 2;
            }
        }
        else
        {
            if (dashTime <= 0)
            {
                directionY = 0;
                directionX = 0;
                dashTime = startDashTime;
                dashing = false;
            }
            else
            {
                dashTime -= Time.deltaTime;

                if (directionY == 1)
                {
                    dashing = true;
                    rb.velocity = new Vector2(rb.velocity.x, dashSpeed);
                }
                else if (directionY == 2)
                {
                    dashing = true;
                    rb.velocity = new Vector2(rb.velocity.x, -dashSpeed);
                }

                if (directionX == 1)
                {
                    dashing = true;
                    rb.velocity = new Vector2(-dashSpeed, rb.velocity.y);
                }
                else if (directionX == 2)
                {
                    dashing = true;
                    rb.velocity = new Vector2(dashSpeed, rb.velocity.y);
                }
            }
        }
    }*/

    void Flip(float speed)
    {
        if (speed > 0 && !facingRight)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
            facingRight = !facingRight;
        }
        if (speed < 0 && facingRight)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
            facingRight = !facingRight;
        }
    }
}