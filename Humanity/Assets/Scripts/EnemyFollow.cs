﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    public float speed;
    private Transform target;
    private bool hitPlayer;
    private Rigidbody2D rb;
    private bool facingRight = true;

    private float previousXPos;
    private float currentXPos;
    private bool canMove = false;

    public GameObject dustEffect;

    public int health;
    private bool isDead = false;
    public GameObject[] blood;
    public float r;
    public float g;
    public float b;

    // Start is called before the first frame update
    void Start()
    {
        GameObject dust = (GameObject) Instantiate(dustEffect, transform.position, Quaternion.identity);
        Destroy(dust, 3f);
        currentXPos = transform.position.x;
        rb = GetComponent<Rigidbody2D>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        GetComponent<BoxCollider2D>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        previousXPos = currentXPos;
        currentXPos = transform.position.x;
        Flip(currentXPos, previousXPos);

        if (!hitPlayer && canMove)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }

        if (health <= 0 && !isDead)
        {
            GameObject bloodSplatter = (GameObject) Instantiate(blood[Random.Range(0, blood.Length - 1)], transform.position, Quaternion.identity);
            bloodSplatter.GetComponent<SpriteRenderer>().color = new Color(r, g, b);
            Destroy(gameObject);
            isDead = true;
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            hitPlayer = true;
        }

        if (other.gameObject.name == "AssaultRifleBullet(Clone)")
        {
            health -= 1;
        }

        if (other.gameObject.name == "SniperRifleBullet(Clone)")
        {
            health -= 9;
        }

        if (other.gameObject.name == "SplashCannonBullet(Clone)")
        {
            health -= 2;
        }
    }

    void Flip(float prevPos, float currPos)
    {
        if (currPos > prevPos && !facingRight)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = false;
            facingRight = !facingRight;
        }
        if (currPos < prevPos && facingRight)
        {
            gameObject.GetComponent<SpriteRenderer>().flipX = true;
            facingRight = !facingRight;
        }
    }

    void CanMove()
    {
        canMove = true;
    }

    void TurnOnCollider()
    {
        GetComponent<BoxCollider2D>().enabled = true;
    }
}
