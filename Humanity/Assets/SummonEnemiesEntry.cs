﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonEnemiesEntry : MonoBehaviour
{
    public GameObject[] enemies;
    private int enemiesPerWave = 5;
    private Vector3 buffer = new Vector3(.5f, .5f, 0f);
    private float roomEdgeX = 4.5f;
    private float roomEdgeY = 3.23f;
    public bool isEntryRoom;

    //private bool firstWaveSpawned = false;
    private bool secondWaveSpawned = false;
    private List<GameObject> wave = new List<GameObject>();
    private int waveCount;
    // Start is called before the first frame update
    void Start()
    {
        // Instantiate 5 enemies
        for (int i = 0; i < enemiesPerWave; i++)
        {
            GameObject spawnedEnemy = (GameObject)Instantiate(enemies[Random.Range(0, enemies.Length - 1)], gameObject.transform.parent.parent.transform.position + new Vector3(Random.Range(-roomEdgeX, roomEdgeX), Random.Range(-roomEdgeY, roomEdgeY), 0), Quaternion.identity);
            wave.Add(spawnedEnemy);
        }
    }

    private void Update()
    {
        // Remove enemy from the wave list when it dies
        for (int i = wave.Count - 1; i >= 0; i--)
        {
            if (wave[i] == null)
            {
                wave.RemoveAt(i);
            }
        }

        // If number of enemies left alive is 1 or less, instantiate 5 more enemies, then don't summon anymore
        if (wave.Count <= 1 && !secondWaveSpawned)
        {
            for (int i = 0; i < enemiesPerWave; i++)
            {
                Instantiate(enemies[Random.Range(0, enemies.Length - 1)], gameObject.transform.parent.parent.transform.position + new Vector3(Random.Range(-roomEdgeX, roomEdgeX), Random.Range(-roomEdgeY, roomEdgeY), 0), Quaternion.identity);
            }

            secondWaveSpawned = true;
        }
    }
}
